import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { listProducts, saveProduct } from "../actions/productsAction";

export default function Products() {
  const productList = useSelector((state) => state.productList);
  const { products, loading, error } = productList;
  const dispatch = useDispatch();
  const [formData, setFormData] = useState({});
  const productSave = useSelector((state) => state.productSave);
  const {
    loading: loadingSave,
    success: successSave,
    error: errorSave,
  } = productSave;
  useEffect(() => {
    dispatch(listProducts());
  }, [successSave]);
  const submitHandler = (e) => {
    e.preventDefault();
    console.log(formData);
    dispatch(saveProduct(formData));
  };
  console.log(products);
  return (
    <div className="container">
      <Link to="/">Back</Link>
      <div className="columns">
        <div className="column is-two-fifths">
          {successSave && <span class="tag is-success">Saved Product</span>}
          <form onSubmit={submitHandler}>
            <div className="field">
              <label className="label">Name</label>
              <div className="control">
                <input
                  className="input"
                  type="text"
                  placeholder="Name"
                  onChange={(e) =>
                    setFormData({ ...formData, name: e.target.value })
                  }
                  value={formData.name}
                />
              </div>
            </div>
            <div className="field">
              <label className="label">Image</label>
              <div className="control">
                <input
                  className="input"
                  type="text"
                  placeholder="Image URL"
                  onChange={(e) =>
                    setFormData({ ...formData, imageUrl: e.target.value })
                  }
                  value={formData.imageUrl}
                />
              </div>
            </div>
            <div className="field">
              <label className="label">Latest Price</label>
              <div className="control">
                <input
                  className="input"
                  type="text"
                  placeholder="Lastest Price"
                  onChange={(e) =>
                    setFormData({ ...formData, latestPrice: e.target.value })
                  }
                  value={formData.latestPrice}
                />
              </div>
            </div>
            <div className="field is-grouped">
              <div className="control">
                <button type="submit" className="button is-link">
                  Submit
                </button>
              </div>
              <div className="control">
                <button className="button is-link is-light">Cancel</button>
              </div>
            </div>
          </form>
        </div>
        <div className="column">
          {loading ? (
            <p>Loading...</p>
          ) : error ? (
            <p>{error}</p>
          ) : (
            <table className="table is-bordered is-striped">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Image</th>
                  <th>Latest Price</th>
                </tr>
              </thead>
              <tbody>
                {products.map(({ id, name, imageUrl, latestPrice }) => (
                  <tr key={id}>
                    <td>{name}</td>
                    <td>
                      <img src={imageUrl} height="40" />
                    </td>
                    <td>{latestPrice}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          )}
        </div>
      </div>
    </div>
  );
}
