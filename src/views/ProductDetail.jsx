import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom";
import { detailsProduct } from "../actions/productsAction";

const ProductDetail = () => {
  const formatPrice = (price) => {
    return Number(price).toLocaleString("vi-VN", {
      style: "currency",
      currency: "VND",
    });
  };
  const productDetails = useSelector((state) => state.productDetails);
  const { product, loading, error } = productDetails;
  const dispatch = useDispatch();
  const { id } = useParams();
  useEffect(() => {
    dispatch(detailsProduct(id));
  }, []);
  return (
    <div>
      <Link to="/">Back</Link>
      {loading ? (
        <p>Loading...</p>
      ) : error ? (
        <p>{error}</p>
      ) : (
        <div>
          <div className="container">
            <div className="card">
              <div className="card-content">
                <img src={product.imageUrl} alt={product.name} />
                <p>{product.name}</p>
                <span className="tag is-info is-large">
                  {formatPrice(product.latestPrice)}
                </span>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default ProductDetail;
