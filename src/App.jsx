import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { listProducts } from "./actions/productsAction";
import "./App.css";

function App() {
  const formatPrice = (price) => {
    return Number(price).toLocaleString("vi-VN", {
      style: "currency",
      currency: "VND",
    });
  };

  const productList = useSelector((state) => state.productList);
  const { products, loading, error } = productList;
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(listProducts());
  }, [dispatch]);

  return loading ? (
    <div>Loading...</div>
  ) : error ? (
    <div>{error}</div>
  ) : (
    <div className="container">
      <Link to="/products">Add Product</Link>
      <div className="columns is-multiline is-desktop">
        {products.map(({ id, name, imageUrl, latestPrice }) => (
          <Link to={`/products/${id}`} key={id} className="column is-3">
            <div className="card">
              <div className="card-content">
                <img src={imageUrl} alt={name} />
                <p>{name.length > 48 ? `${name.substring(0, 48)}...` : name}</p>
                <span className="tag is-info is-large">
                  {formatPrice(latestPrice)}
                </span>
              </div>
            </div>
          </Link>
        ))}
      </div>
    </div>
  );
}

export default App;
